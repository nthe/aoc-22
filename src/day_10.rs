#![allow(unused)]

use itertools::Itertools;

enum Op {
    Noop,
    Add(isize),
}

impl From<&str> for Op {
    fn from(value: &str) -> Self {
        match value.split_once(' ') {
            Some(("addx", num)) => Op::Add(num.parse::<isize>().unwrap()),
            _ => Op::Noop,
        }
    }
}

pub fn part_one(input: &str) -> isize {
    let mut reg: isize = 1;
    let mut acc: isize = 0;
    let mut mem: isize = 0;
    let mut on_hold: bool = true;
    let mut ops = input.lines().rev().map_into().collect::<Vec<Op>>();

    for i in 0..220 {
        if (i + 1 - 20) % 40 == 0 {
            acc += reg * (i + 1);
        }
        if on_hold {
            match ops.pop().unwrap() {
                Op::Add(n) => {
                    mem = n;
                    on_hold = false;
                }
                _ => {}
            }
        } else {
            reg += mem;
            mem = 0;
            on_hold = true;
        }
    }

    acc
}

fn print_screen(screen: &Vec<(isize, isize)>) {
    for i in 1..screen.len() {
        if screen[i].1 != screen[i - 1].1 {
            println!();
        }
        for _ in 1..screen[i].0.abs_diff(screen[i - 1].0) {
            print!(" ");
        }
        print!("#");
    }
    println!();
}

pub fn part_two(input: &str) -> Vec<(isize, isize)> {
    let mut reg: isize = 1;
    let mut mem: isize = 0;
    let mut on_hold: bool = true;
    let mut ops = input.lines().rev().map_into().collect::<Vec<Op>>();

    let mut screen = vec![];

    for i in 0..240 {
        if reg.abs_diff(i % 40) <= 1 {
            screen.push((i % 40, i / 40));
        }

        if on_hold {
            match ops.pop().unwrap() {
                Op::Add(n) => {
                    mem = n;
                    on_hold = false;
                }
                _ => {}
            }
        } else {
            reg += mem;
            mem = 0;
            on_hold = true;
        }
    }

    screen
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/10/input");
        let out = part_one(input);
        assert_eq!(16020, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/10/input");
        let out = part_two(input);
        print_screen(&out);
        assert!(!out.is_empty());
    }
}
