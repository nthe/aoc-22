#![allow(unused)]

use std::collections::HashSet;

enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl From<&str> for Direction {
    fn from(value: &str) -> Self {
        match value {
            "U" => Direction::Up,
            "R" => Direction::Right,
            "D" => Direction::Down,
            "L" => Direction::Left,
            _ => unreachable!(),
        }
    }
}

struct Move {
    direction: Direction,
    steps: u8,
}

#[derive(Default)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn step(&mut self, direction: &Direction) {
        match direction {
            Direction::Up => self.y += 1,
            Direction::Down => self.y -= 1,
            Direction::Right => self.x += 1,
            Direction::Left => self.x -= 1,
        }
    }

    fn follow(&mut self, head: &Position) {
        let delta = (head.x - self.x, head.y - self.y);
        if delta.0.abs() == 2 || delta.1.abs() == 2 {
            self.x += delta.0.signum();
            self.y += delta.1.signum();
        }
    }
}

fn parse_moves(value: &str) -> Vec<Move> {
    value
        .lines()
        .filter_map(|l| l.split_once(' '))
        .map(|(d, s)| Move {
            direction: d.into(),
            steps: s.parse().unwrap(),
        })
        .collect()
}

fn simulate(input: &str, tail_knots: u8) -> usize {
    let mut head = Position::default();
    let mut smem = HashSet::from([(0, 0)]);

    let mut tails: Vec<Position> = vec![];
    for _ in 0..tail_knots {
        tails.push(Position::default());
    }

    for m in parse_moves(input) {
        for _ in 0..m.steps {
            head.step(&m.direction);
            let mut prev = &head;
            for t in tails.iter_mut() {
                t.follow(prev);
                prev = t;
            }
            let last = tails.last().unwrap();

            smem.insert((last.x, last.y));
        }
    }

    smem.len()
}

pub fn part_one(input: &str) -> usize {
    simulate(input, 1)
}

pub fn part_two(input: &str) -> usize {
    simulate(input, 9)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/09/input");
        let out = part_one(input);
        assert_eq!(5902, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/09/input");
        let out = part_two(input);
        assert_eq!(2445, out);
    }
}
