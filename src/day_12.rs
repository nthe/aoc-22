#![allow(unused)]

use std::collections::{HashSet, VecDeque};

#[derive(Copy, Clone, Debug, Default, Eq, Hash, PartialEq)]
struct Point(usize, usize);

impl Point {
    #[rustfmt::skip]
    fn neighbours(&self, w: usize, h: usize) -> Vec<Option<Point>> {
        vec![
            if self.0 + 1 < w { Some(Point(self.0 + 1, self.1)) } else { None },
            if self.1 + 1 < h { Some(Point(self.0, self.1 + 1)) } else { None },
            if self.0 > 0 { Some(Point(self.0 - 1, self.1)) } else { None },
            if self.1 > 0 { Some(Point(self.0, self.1 - 1)) } else { None },
        ]
    }
}

impl From<(usize, usize)> for Point {
    fn from((x, y): (usize, usize)) -> Self {
        Self(x, y)
    }
}

fn as_u32(c: char) -> i32 {
    match c {
        'a'..='z' => c as i32,
        'S' => 'a' as i32,
        'E' => 'z' as i32,
        _ => panic!("{c}"),
    }
}

fn solve(input: &str, dst_char: char) -> Option<usize> {
    let mut src: Point = Point::default();
    let mut dst: Point = Point::default();

    let map: Vec<Vec<char>> = input
        .lines()
        .enumerate()
        .map(|(x, line)| {
            line.chars()
                .enumerate()
                .map(|(y, ch)| {
                    match ch {
                        'S' => src = Point(x, y),
                        'E' => dst = Point(x, y),
                        _ => {}
                    }
                    ch
                })
                .collect()
        })
        .collect();

    let mut memo: HashSet<Point> = HashSet::default();
    let mut buff: VecDeque<(Point, usize)> = VecDeque::from([(dst, 0)]);
    let mut routes: Vec<usize> = Vec::default();

    let max_row: usize = map.len();
    let max_col: usize = map.get(0).expect("map to contain any data").len();

    while let Some((pos, steps)) = buff.pop_front() {
        if memo.contains(&pos) {
            continue;
        }
        memo.insert(pos);

        let height: char = map[pos.0][pos.1];
        if height == dst_char {
            routes.push(steps);
        }

        pos.neighbours(max_row, max_col)
            .into_iter()
            .filter_map(|n| n)
            .filter(|n| as_u32(height) - as_u32(map[n.0][n.1]) < 2)
            .for_each(|n| buff.push_back((n, steps + 1)));
    }

    routes.into_iter().min()
}

pub fn part_one(input: &str) -> Option<usize> {
    solve(input, 'S')
}

pub fn part_two(input: &str) -> Option<usize> {
    solve(input, 'a')
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/12/input");
        let out = part_one(input);
        assert_eq!(Some(484), out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/12/input");
        let out = part_two(input);
        assert_eq!(Some(478), out);
    }
}
