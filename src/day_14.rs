use std::{collections::HashMap, fmt::Display};

#[derive(Debug)]
enum Tile {
    Air,
    Rock,
    Sand,
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
struct Point(usize, usize);

impl Point {
    fn as_line_to(&self, b: &Point) -> Vec<Point> {
        let a = self;
        match (b.0 as isize - a.0 as isize, b.1 as isize - a.1 as isize) {
            (0, 0) => vec![Point(a.0, a.1)],
            (0, d) => match d > 0 {
                true => (a.1..=b.1).map(|y| Point(a.0, y)).collect(),
                false => (b.1..=a.1).map(|y| Point(a.0, y)).collect(),
            },
            (d, 0) => match d > 0 {
                true => (a.0..=b.0).map(|x| Point(x, b.1)).collect(),
                false => (b.0..=a.0).map(|x| Point(x, b.1)).collect(),
            },
            (_, _) => panic!("not a line"),
        }
    }

    fn lookup_tiles(&self) -> Vec<Point> {
        vec![
            Point(self.0, self.1 + 1),
            Point(self.0 - 1, self.1 + 1),
            Point(self.0 + 1, self.1 + 1),
        ]
    }
}

impl From<&str> for Point {
    fn from(value: &str) -> Self {
        let (x, y) = value.split_once(',').expect("tuple string");
        Self(
            x.parse().expect("x coord to be usize"),
            y.parse().expect("y coord to be usize"),
        )
    }
}

#[derive(Debug)]
struct RockPath(Vec<Point>);

impl From<&str> for RockPath {
    fn from(value: &str) -> Self {
        Self(value.split(" -> ").map(|p| p.into()).collect())
    }
}

struct Map {
    tiles: HashMap<Point, Tile>,
    min_x: usize,
    max_x: usize,
    max_y: usize,
}

impl From<Vec<&Point>> for Map {
    fn from(points: Vec<&Point>) -> Self {
        let min_x: usize = points.iter().map(|p| p.0).min().expect("any points");
        let max_x: usize = points.iter().map(|p| p.0).max().expect("any points");
        let max_y: usize = points.iter().map(|p| p.1).max().expect("any points");

        let mut tiles: HashMap<Point, Tile> = HashMap::default();

        for i in min_x..=max_x {
            for j in 0..=max_y {
                tiles.insert(Point(i, j), Tile::Air);
            }
        }

        Self {
            tiles,
            min_x,
            max_x,
            max_y,
        }
    }
}

impl Map {
    fn fill(&mut self, paths: Vec<RockPath>) {
        for path in paths {
            for i in 1..path.0.len() {
                let p = &path.0[i - 1];
                let n = &path.0[i];
                for m in p.as_line_to(n) {
                    self.tiles.insert(m, Tile::Rock);
                }
            }
        }
    }

    fn next_vacant_under(&mut self, sand: &Point) -> Option<Point> {
        for p in sand.lookup_tiles() {
            let Some(t) = self.tiles.get(&p) else {
                return Some(p);
            };
            match t {
                Tile::Air => return Some(p),
                _ => continue,
            }
        }
        None
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for i in 0..=self.max_y {
            for j in self.min_x..=self.max_x {
                match self.tiles.get(&Point(j, i)) {
                    Some(Tile::Rock) => write!(f, "# "),
                    Some(Tile::Sand) => write!(f, "o "),
                    _ => write!(f, ". "),
                }?
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

pub fn part_one(input: &str) -> usize {
    let paths: Vec<RockPath> = input.split('\n').map(|line| line.into()).collect();
    let points: Vec<&Point> = paths.iter().flat_map(|p| p.0.iter()).collect();

    let mut map: Map = points.into();
    map.fill(paths);

    let mut sand: Point = Point(500, 0);
    let mut units: usize = 0;
    loop {
        let nv = map.next_vacant_under(&sand);
        match nv {
            Some(p) => {
                if p.0 < map.min_x || p.0 > map.max_x || p.1 > map.max_y {
                    break;
                }
                map.tiles.insert(p, Tile::Sand);
                map.tiles.insert(sand, Tile::Air);
                sand = p;
            }
            None => {
                units += 1;
                sand = Point(500, 0);
            }
        }
    }
    // println!("[the map]");
    // println!("{}", map);

    units
}

pub fn part_two(input: &str) -> usize {
    let mut paths: Vec<RockPath> = input.split('\n').map(|line| line.into()).collect();
    let points: Vec<&Point> = paths.iter().flat_map(|p| p.0.iter()).collect();
    let mut map: Map = points.into();

    paths.push(RockPath(vec![
        Point(0, map.max_y + 2),
        Point(1000, map.max_y + 2),
    ]));
    map.fill(paths);

    let mut stuck: bool = true;
    let mut sand: Point = Point(500, 0);
    let mut units: usize = 1;
    loop {
        let nv = map.next_vacant_under(&sand);
        match nv {
            Some(p) => {
                stuck = false;
                if p.0 < map.min_x {
                    map.min_x = p.0;
                }
                if p.0 > map.max_x {
                    map.max_x = p.0;
                }
                map.tiles.insert(p, Tile::Sand);
                map.tiles.insert(sand, Tile::Air);
                sand = p;
            }
            None => {
                if stuck {
                    break;
                }
                stuck = true;
                units += 1;
                sand = Point(500, 0);
            }
        }
    }

    units
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/14/test");
        let out = part_one(input);
        assert_eq!(24, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/14/test");
        let out = part_two(input);
        assert_eq!(93, out);
    }
}
