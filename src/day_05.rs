#![allow(unused)]

use std::{collections::HashMap, vec};

trait PostParser {
    fn unpack(&self, label: &str) -> usize;
}

impl PostParser for Option<&&str> {
    fn unpack(&self, label: &str) -> usize {
        self.expect(&format!("{} var", label))
            .parse()
            .expect(&format!("{} to be usize", label))
    }
}

struct Move {
    count: usize,
    from: usize,
    to: usize,
}

impl From<&str> for Move {
    fn from(s: &str) -> Self {
        let words: Vec<&str> = s.split(' ').collect();
        let count: usize = words.get(1).unpack("count");
        let from: usize = words.get(3).unpack("from");
        let to: usize = words.get(5).unpack("to");
        Self { count, from, to }
    }
}

fn parse_crates(text: &str) -> HashMap<usize, Vec<char>> {
    let mut stacks = HashMap::<usize, Vec<char>>::new();
    let mut index_to_col = HashMap::<usize, usize>::new();

    for (i, c) in text.lines().last().unwrap().chars().enumerate() {
        if c.is_numeric() {
            let ci = c.to_string().parse::<usize>().unwrap();
            stacks.insert(ci, vec![]);
            index_to_col.insert(i, ci);
        }
    }

    for line in text.lines().rev().skip(1) {
        for (i, c) in line.chars().enumerate() {
            if c.is_alphabetic() {
                let p = index_to_col.get(&i).unwrap();
                stacks.get_mut(p).unwrap().push(c);
            }
        }
    }

    stacks
}

struct InputFile {
    crates: HashMap<usize, Vec<char>>,
    moves: Vec<Move>,
}

impl From<&str> for InputFile {
    fn from(text: &str) -> Self {
        let (crates_text, moves_text) = text.split_once("\n\n").expect("correct input file format");

        let crates: HashMap<usize, Vec<char>> = parse_crates(crates_text);
        let moves: Vec<Move> = moves_text.lines().map(|line| line.into()).collect();

        Self { crates, moves }
    }
}

pub fn part_one(input: &str) -> String {
    let mut con: InputFile = input.into();

    for m in con.moves {
        for _ in 0..m.count {
            let x = con.crates.get_mut(&m.from).unwrap().pop().unwrap();
            con.crates.get_mut(&m.to).unwrap().push(x);
        }
    }

    let x = *con.crates.keys().max().unwrap();

    let o = (1..=x)
        .map(|i| con.crates.get(&i).unwrap().last().unwrap())
        .collect::<String>();

    o
}

pub fn part_two(input: &str) -> String {
    let mut con: InputFile = input.into();

    for m in con.moves {
        let src = con.crates.get_mut(&m.from).unwrap();
        let mut mem: Vec<char> = (0..m.count).map(|_| src.pop().unwrap()).collect();
        mem.reverse();
        con.crates.get_mut(&m.to).unwrap().extend(mem);
    }

    let x = *con.crates.keys().max().unwrap();

    let o = (1..=x)
        .map(|i| con.crates.get(&i).unwrap().last().unwrap())
        .collect::<String>();

    o
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/05/input");
        let out = part_one(input);
        assert_eq!("BWNCQRMDB", out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/05/input");
        let out = part_two(input);
        assert_eq!("NHWZCBNBF", out);
    }
}
