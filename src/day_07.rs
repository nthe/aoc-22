#![allow(unused)]

pub fn part_one(input: &str) -> usize {
    0
}

pub fn part_two(input: &str) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        todo!();
    }

    #[test]
    fn test_part_two() {
        todo!();
    }
}
