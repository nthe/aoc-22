#![allow(unused)]

enum Direction {
    N,
    W,
    S,
    E,
}

impl Direction {
    fn list() -> [Self; 4] {
        [Self::N, Self::W, Self::S, Self::E]
    }

    fn update(&self, x: usize, y: usize) -> (usize, usize) {
        match self {
            Direction::N => (x, y + 1),
            Direction::W => (x - 1, y),
            Direction::S => (x, y - 1),
            Direction::E => (x + 1, y),
        }
    }
}

fn is_visible(
    m: &Vec<Vec<u32>>,
    z: u32,
    x: usize,
    y: usize,
    direction: Option<&Direction>,
) -> bool {
    let w: usize = m.len() - 1;
    let h: usize = m[0].len() - 1;

    if x == 0 || x == w || y == 0 || y == h {
        return true;
    }

    if let Some(dir) = direction {
        let (j, k) = dir.update(x, y);
        if m[j][k] >= z {
            return false;
        } else {
            return is_visible(m, z, j, k, direction);
        }
    }

    Direction::list()
        .iter()
        .any(|d| is_visible(m, z, x, y, Some(d)))
}

fn scenic_score(
    m: &Vec<Vec<u32>>,
    z: u32,
    x: usize,
    y: usize,
    direction: Option<&Direction>,
) -> u64 {
    let w: usize = m.len() - 1;
    let h: usize = m[0].len() - 1;

    if x == 0 || x == w || y == 0 || y == h {
        return 0;
    }

    if let Some(dir) = direction {
        let (j, k) = dir.update(x, y);
        if m[j][k] >= z {
            return 1;
        } else {
            return 1 + scenic_score(m, z, j, k, direction);
        }
    }

    Direction::list()
        .iter()
        .map(|d| scenic_score(m, z, x, y, Some(d)))
        .product()
}

fn parse_map(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_string().parse::<u32>().unwrap())
                .collect()
        })
        .collect()
}

pub fn part_one(input: &str) -> usize {
    let m: Vec<Vec<u32>> = parse_map(input);
    let mut visible_count: usize = 0;
    for i in 0..m.len() {
        for j in 0..m[0].len() {
            if is_visible(&m, m[i][j], i, j, Option::None) {
                visible_count += 1;
            }
        }
    }
    visible_count
}

pub fn part_two(input: &str) -> u64 {
    let m: Vec<Vec<u32>> = parse_map(input);
    let mut scores: Vec<u64> = vec![];
    for i in 0..m.len() {
        for j in 0..m[0].len() {
            scores.push(scenic_score(&m, m[i][j], i, j, Option::None));
        }
    }
    *scores.iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/08/input");
        let out = part_one(input);
        assert_eq!(1820, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/08/input");
        let out = part_two(input);
        assert_eq!(385112, out);
    }
}
