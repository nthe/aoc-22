#![allow(unused)]

use std::ops::RangeInclusive;

struct Space {
    from: u32,
    to: u32,
}

impl Space {
    pub fn size(&self) -> u32 {
        self.to - self.from
    }

    pub fn as_range(&self) -> RangeInclusive<u32> {
        self.from..=self.to
    }

    pub fn overlap(&self, other: Space) -> bool {
        let ts = self.as_range();
        other.as_range().any(|i| ts.contains(&i))
    }

    pub fn contains(&self, other: Space) -> bool {
        let (mut os, ts) = match self.size() > other.size() {
            true => (other.as_range(), self.as_range()),
            false => (self.as_range(), other.as_range()),
        };

        os.all(|i| ts.contains(&i))
    }
}

impl From<&str> for Space {
    fn from(value: &str) -> Self {
        let (from, to) = value.split_once('-').expect("Error");
        Space {
            from: from.parse().expect("an integer"),
            to: to.parse().expect("an integet"),
        }
    }
}

pub fn part_one(input: &str) -> usize {
    input
        .lines()
        .map(|line| line.split_once(',').unwrap())
        .filter(|&(a, b)| Space::from(a).contains(Space::from(b)))
        .count()
}

pub fn part_two(input: &str) -> usize {
    input
        .lines()
        .map(|line| line.split_once(',').unwrap())
        .filter(|&(a, b)| Space::from(a).overlap(Space::from(b)))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/04/input");
        let out = part_one(input);
        assert_eq!(483, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/04/input");
        let out = part_two(input);
        assert_eq!(874, out);
    }
}
