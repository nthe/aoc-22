#![allow(unused)]

use itertools::Itertools;

#[derive(Debug)]
enum Operation {
    Add(isize),
    Mult(isize),
    Sqr,
}

#[derive(Debug)]
struct Monkey {
    items: Vec<isize>,
    operation: Operation,
    div_test: isize,
    on_success: isize,
    on_failure: isize,
    counter: isize,
}

impl Monkey {
    fn inspect(&mut self, f: &(dyn Fn(isize) -> isize)) -> Vec<(isize, isize)> {
        let mut distribution = vec![];

        for old in self.items.iter() {
            let mut new = match self.operation {
                Operation::Add(num) => old + num,
                Operation::Mult(num) => old * num,
                Operation::Sqr => old * old,
            };

            new = f(new);

            if new % self.div_test == 0 {
                distribution.push((new, self.on_success));
            } else {
                distribution.push((new, self.on_failure));
            }
            self.counter += 1;
        }

        self.items.clear();
        distribution
    }
}

fn parse_items<'a>(terms: &[&'a str]) -> Vec<isize> {
    terms
        .iter()
        .map(|t| t.replace(',', ""))
        .map(|t| t.parse().unwrap())
        .collect()
}

fn parse_operation<'a>(terms: &[&'a str]) -> Operation {
    match terms {
        &["new", "=", "old", op, "old"] => match op {
            "*" => Operation::Sqr,
            "+" => Operation::Mult(2),
            _ => panic!("{op}"),
        },
        &["new", "=", "old", op, num] => match op {
            "*" => Operation::Mult(parse_isize(num)),
            "+" => Operation::Add(parse_isize(num)),
            _ => panic!("{op} {num}"),
        },
        _ => panic!("{:?}", terms),
    }
}

fn parse_isize(terms: &str) -> isize {
    terms.parse().unwrap()
}

impl From<&str> for Monkey {
    fn from(value: &str) -> Self {
        let mut items = vec![];
        let mut operation = Operation::Add(0);
        let mut test = 0;
        let mut on_success = 0;
        let mut on_failure = 0;

        for line in value.lines() {
            let terms = line.split_whitespace().collect::<Vec<_>>();
            match terms[0] {
                "Monkey" => {}
                "Starting" => items = parse_items(&terms[2..]),
                "Operation:" => operation = parse_operation(&terms[1..]),
                "Test:" => test = parse_isize(&terms[3]),
                "If" => match terms[1] {
                    "true:" => on_success = parse_isize(&terms[5]),
                    "false:" => on_failure = parse_isize(&terms[5]),
                    _ => {}
                },
                _ => panic!("{}", terms[0]),
            }
        }

        Self {
            items,
            operation,
            div_test: test,
            on_success,
            on_failure,
            counter: 0,
        }
    }
}

fn solve(monkeys: &mut Vec<Monkey>, rounds: isize, f: &(dyn Fn(isize) -> isize)) -> isize {
    for _ in 0..rounds {
        for i in 0..monkeys.len() {
            let dist = monkeys[i].inspect(f);
            for (v, t) in dist {
                monkeys[t as usize].items.push(v);
            }
        }
    }

    monkeys
        .iter()
        .map(|m| m.counter)
        .sorted()
        .rev()
        .take(2)
        .product()
}

pub fn part_one(input: &str) -> isize {
    let mut monkeys: Vec<Monkey> = input.split("\n\n").map_into().collect();
    solve(&mut monkeys, 20, &|n| n / 3)
}

pub fn part_two(input: &str) -> isize {
    let mut monkeys: Vec<Monkey> = input.split("\n\n").map_into().collect();
    let denom: isize = monkeys.iter().map(|m| m.div_test).product();
    solve(&mut monkeys, 10000, &|n| n % denom)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/11/input");
        let out = part_one(input);
        assert_eq!(78678, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/11/input");
        let out = part_two(input);
        assert_eq!(15333249714, out);
    }
}
