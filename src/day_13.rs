#![allow(unused)]

use std::{cmp::Ordering, iter::Peekable, str::Chars};

use itertools::Itertools;

// Packet

#[derive(Debug, Eq)]
enum Packet {
    Number(usize),
    List(Vec<Packet>),
}

// ordering traits @ Packet

impl PartialEq for Packet {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::List(a), Self::List(b)) => a == b,
            (Self::Number(a), Self::Number(b)) => a == b,
            (Self::List(a), Self::Number(b)) => a == &vec![Packet::Number(*b)],
            (Self::Number(a), Self::List(b)) => b == &vec![Packet::Number(*a)],
        }
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            // order of vars in RHS matters!
            (Self::List(a), Self::List(b)) => a.cmp(b),
            (Self::Number(a), Self::Number(b)) => a.cmp(b),
            (Self::List(a), Self::Number(b)) => a.cmp(&vec![Packet::Number(*b)]),
            (Self::Number(a), Self::List(b)) => vec![Packet::Number(*a)].cmp(b),
        }
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

// conversion traits @ Packet

impl From<&str> for Packet {
    fn from(value: &str) -> Self {
        parse(&mut value.chars().peekable())
    }
}

// Pair

#[derive(Debug, PartialEq)]
struct Pair(Packet, Packet);

impl Pair {
    fn in_right_order(&self) -> bool {
        self.0.cmp(&self.1) == Ordering::Less
    }
}

// conversion traits @ Pair

impl From<&str> for Pair {
    fn from(value: &str) -> Self {
        let Some((a, b)) = value.split_once('\n') else {
            panic!("cannot construct Pair from '{value}'");
        };
        Self(a.into(), b.into())
    }
}

fn parse(chars: &mut Peekable<Chars>) -> Packet {
    match chars.peek() {
        Some('[') => {
            chars.next();
            let mut buff = vec![];

            while let Some(n) = chars.peek() {
                if n == &']' {
                    chars.next();
                    return Packet::List(buff);
                }
                buff.push(parse(chars));
                if chars.peek() == Some(&',') {
                    chars.next();
                }
            }

            panic!("unclosed array in buff: {:?}", buff);
        }

        Some(_) => {
            let num: usize = chars
                .peeking_take_while(|c| c.is_ascii_digit())
                .collect::<String>()
                .parse::<usize>()
                .unwrap();

            return Packet::Number(num);
        }

        None => panic!("nothing to peek at"),
    }
}

pub fn part_one(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|c| c.into())
        .collect::<Vec<Pair>>()
        .iter()
        .enumerate()
        .filter(|(_, pair)| pair.in_right_order())
        .map(|(i, _)| i + 1)
        .sum()
}

pub fn part_two(input: &str) -> usize {
    let dec_2 = Packet::List(vec![Packet::List(vec![Packet::Number(2)])]);
    let dec_6 = Packet::List(vec![Packet::List(vec![Packet::Number(6)])]);

    input
        .split("\n\n")
        .map(|p| Pair::from(p))
        .collect::<Vec<Pair>>()
        .iter()
        .flat_map(|Pair(a, b)| [a, b])
        .chain([&dec_2, &dec_6])
        .sorted()
        .enumerate()
        .filter(|(i, p)| p == &&dec_2 || p == &&dec_6)
        .map(|(i, _)| i + 1)
        .product()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/13/input");
        let out = part_one(input);
        assert_eq!(6395, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/13/input");
        let out = part_two(input);
        assert_eq!(24921, out);
    }
}
