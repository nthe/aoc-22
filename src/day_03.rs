#![allow(unused)]

use itertools::Itertools;

pub fn part_one(input: &str) -> u32 {
    let mut part1_score: u32 = 0;

    for line in input.lines() {
        let (left, right) = line.split_at(line.len() / 2);

        let word_score: u32 = left
            .chars()
            .filter(|&c| right.find(c).is_some())
            .unique()
            .map(|c| match c {
                'a'..='z' => c as u32 - 96,
                'A'..='Z' => c as u32 - 65 + 27,
                _ => unreachable!(),
            })
            .sum();

        part1_score += word_score;
    }
    part1_score
}

pub fn part_two(input: &str) -> u32 {
    let mut part2_score: u32 = 0;

    for chunk in &input.lines().chunks(3) {
        let chks = chunk.collect::<Vec<_>>();
        let a = *chks.get(0).unwrap();
        let b = *chks.get(1).unwrap();
        let c = *chks.get(2).unwrap();

        'main: for i in a.chars() {
            for j in b.chars() {
                for k in c.chars() {
                    if i == j && j == k {
                        let y = match i {
                            'a'..='z' => i as u32 - 96,
                            'A'..='Z' => i as u32 - 65 + 27,
                            _ => unreachable!(),
                        };
                        part2_score += y;
                        break 'main;
                    }
                }
            }
        }
    }

    part2_score
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/03/input");
        let out = part_one(input);
        assert_eq!(8085, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/03/input");
        let out = part_two(input);
        assert_eq!(2515, out);
    }
}
