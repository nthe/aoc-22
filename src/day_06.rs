#![allow(unused)]

use itertools::Itertools;

fn decoder(input: &str, buffer: usize) -> usize {
    let mut mem = Vec::<char>::new();

    for (i, c) in input.chars().enumerate() {
        if i > (buffer - 1) {
            mem.remove(0);
        }
        mem.push(c);
        if mem.iter().unique().collect_vec().len() == buffer {
            return i + 1;
        }
    }
    0
}

pub fn part_one(input: &str) -> usize {
    decoder(input, 4)
}

pub fn part_two(input: &str) -> usize {
    decoder(input, 14)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/06/input");
        let out = part_one(input);
        assert_eq!(1766, out);

        assert_eq!(5, part_one("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        assert_eq!(6, part_one("nppdvjthqldpwncqszvftbrmjlhg"));
        assert_eq!(10, part_one("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        assert_eq!(11, part_one("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/06/input");
        let out = part_two(input);
        assert_eq!(2383, out);

        assert_eq!(23, part_two("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        assert_eq!(23, part_two("nppdvjthqldpwncqszvftbrmjlhg"));
        assert_eq!(29, part_two("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        assert_eq!(26, part_two("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
    }
}
