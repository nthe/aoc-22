#![allow(unused)]

use std::{fmt::Debug, iter::Sum, str::FromStr};

use num_traits::Num;

pub trait StrNum: Copy + FromStr + Num + Ord + Sum + Sum<Self> {}

impl<T> StrNum for T where T: Copy + FromStr + Num + Ord + Sum + Sum<T> {}

pub fn part_one<T: StrNum>(text: &str) -> T
where
    <T as FromStr>::Err: Debug,
{
    let mut items = text
        .split("\n\n")
        .map(|c| c.lines().map(|s| s.parse::<T>().unwrap()).sum())
        .collect::<Vec<T>>();

    items.sort();

    let default = T::zero();
    let t = items.last().unwrap_or(&default);

    *t
}

pub fn part_two<T: StrNum>(text: &str) -> T
where
    <T as FromStr>::Err: Debug,
{
    let mut items = text
        .split("\n\n")
        .map(|c| c.lines().map(|s| s.parse::<T>().unwrap()).sum())
        .collect::<Vec<T>>();

    items.sort();

    let tt: T = items[items.len() - 3..]
        .iter()
        .fold(T::zero(), |a, &i| a + i);

    tt
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/01/input");
        let out: i32 = part_one(input);
        assert_eq!(71471, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/01/input");
        let out: i32 = part_two(input);
        assert_eq!(211189, out);
    }
}
