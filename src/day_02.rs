#![allow(unused)]

use std::collections::HashMap;

#[derive(PartialEq)]
enum Outcome {
    Win = 6,
    Draw = 3,
    Loss = 0,
}

#[derive(Copy, Clone)]
enum Symbol {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

const SYMBOLS: &[Symbol] = &[Symbol::Rock, Symbol::Paper, Symbol::Scissors];

impl Symbol {
    pub fn against(&self, other: &Symbol) -> Outcome {
        match (self, other) {
            (Symbol::Rock, Symbol::Rock) => Outcome::Draw,
            (Symbol::Rock, Symbol::Paper) => Outcome::Loss,
            (Symbol::Rock, Symbol::Scissors) => Outcome::Win,
            (Symbol::Paper, Symbol::Paper) => Outcome::Draw,
            (Symbol::Paper, Symbol::Rock) => Outcome::Win,
            (Symbol::Paper, Symbol::Scissors) => Outcome::Loss,
            (Symbol::Scissors, Symbol::Scissors) => Outcome::Draw,
            (Symbol::Scissors, Symbol::Rock) => Outcome::Loss,
            (Symbol::Scissors, Symbol::Paper) => Outcome::Win,
        }
    }
}

type Strategy = HashMap<char, Symbol>;

fn as_char(s: &str) -> char {
    s.to_lowercase().chars().nth(0).unwrap()
}

fn part_one_cmp(l: &str, ls: &Strategy, r: &str, rs: &Strategy) -> u32 {
    let l = as_char(l);
    let r = as_char(r);
    let a = &ls[&l];
    let b = &rs[&r];
    let o = b.against(a);
    (o as u32) + (*b as u32)
}

pub fn part_one(input: &str) -> u32 {
    let elfs = Strategy::from([
        ('a', Symbol::Rock),
        ('b', Symbol::Paper),
        ('c', Symbol::Scissors),
    ]);

    let mine = Strategy::from([
        ('x', Symbol::Rock),
        ('y', Symbol::Paper),
        ('z', Symbol::Scissors),
    ]);

    let lines: Vec<_> = input
        .lines()
        .map(|line| line.split_once(' ').unwrap())
        .collect();

    let p1: u32 = lines
        .iter()
        .map(|(a, b)| part_one_cmp(a, &elfs, b, &mine))
        .sum();

    p1
}

fn part_two_cmp(l: &str, ls: &Strategy, r: &str) -> u32 {
    let l = as_char(l);
    let r = as_char(r);
    let a = &ls[&l];
    let o = match r {
        'x' => Outcome::Loss,
        'y' => Outcome::Draw,
        'z' => Outcome::Win,
        _ => unreachable!(),
    };
    let b = SYMBOLS.iter().find(|&s| s.against(a) == o).unwrap();
    (o as u32) + (*b as u32)
}

pub fn part_two(input: &str) -> u32 {
    let elfs = Strategy::from([
        ('a', Symbol::Rock),
        ('b', Symbol::Paper),
        ('c', Symbol::Scissors),
    ]);

    let lines: Vec<_> = input
        .lines()
        .map(|line| line.split_once(' ').unwrap())
        .collect();

    let p2: u32 = lines.iter().map(|(a, b)| part_two_cmp(a, &elfs, b)).sum();

    p2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = include_str!("../assets/02/input");
        let out = part_one(input);
        assert_eq!(8890, out);
    }

    #[test]
    fn test_part_two() {
        let input = include_str!("../assets/02/input");
        let out = part_two(input);
        assert_eq!(10238, out);
    }
}
